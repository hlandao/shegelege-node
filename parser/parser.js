"use strict";

var lineReader = require('line-reader');
var Location = require('../models/location')
var Warehouse = require('../models/warehouse')
var Order = require('../models/order')
var Game = require('../models/game')
var Item = require('../models/item')
var Drone = require('../models/drone')


var linesPerWarehouse = 2
var linesPerOrder = 3


function readFile(filename, done) {
    var output = []
    lineReader.eachLine(filename, function(line, last) {
        output.push(line);
    }, function() {
        done(null, output);
    });
}


function parseGameMetaData(metaDataLines) {

    let data = parseFirstLine(metaDataLines[0]);
    let productsAmount = parseSecondLine(metaDataLines[1]);
    let productsWeights = parseThirdLine(metaDataLines[2]);
    let numberOfWarehouses =  parseFourthLine(metaDataLines[3]);

    return new Game(data.rows, data.cols, data.numberOfDrones, data.turns, data.maxPayload, productsAmount, productsWeights, numberOfWarehouses);
}

//// PARSE META DATA ////
// parse meta 1st line = game
function parseFirstLine(line) {
    let arr = line.split(" ");
    return {
        rows : arr[0],
        cols : arr[1],
        numberOfDrones : arr[2],
        turns : arr[3],
        maxPayload : arr[4]
    }
}

// parse meta 2nd line = productsAmount
function parseSecondLine(line) {
    return line;
}

// parse meta 3rd line = productsWeight
function parseThirdLine(line) {
    let arr = line.split(" ");
    return arr;
}

// parse meta 4th line = amountOfWarehouses
function parseFourthLine(line) {
    return line;
}

//// PARSE WAREHOUSES DATA ////
function parseWarehouses(warehousesLines) {
    var warehouses = []
    for (let i = 0; i < warehousesLines.length ; ++i) {

        var warehouseNumber = Math.floor(i % linesPerWarehouse);
        if (i % linesPerWarehouse == 0) {
            let location =  parseWarehouseFirstLine(warehousesLines[i]);
            warehouses.push(new Warehouse(warehouseNumber, location))
        } else {
            let inventory = parseWarehouseSecondLine(warehousesLines[i])
            let warehouseIndex = Math.floor(i/2);
            warehouses[warehouseIndex].inventory = inventory
        }
    }
    return warehouses;
}

// parse warehouse 1st line = location
function parseWarehouseFirstLine(line) {
    let arr = line.split(" ");
    return new Location(arr[0], arr[1]);
}

// parse warehouse 2nd line = inventory
function parseWarehouseSecondLine(line) {
    let arr = line.split(" ");
    return arr;
}

//// PARSE ORDERS DATA ////
function parseOrders(ordersLines, itemsWeights) {
    var orders = []
    for (let i = 0; i < ordersLines.length ; ++i) {
        let orderNumber = Math.floor(i / 3);
        let j = i % linesPerOrder;
        if(!orders[orderNumber]) {
            orders[orderNumber] = new Order(orderNumber);
        }
        let order = orders[orderNumber];
        switch (j) {
            case 0:
                order.destination = parseOrderFirstLine(ordersLines[i]);
                break;
            case 1:
                order.numberOfItems = parseOrderSecondLine(ordersLines[i]);
                break;
            case 2:
                order.itemsToDeliver = parseOrderThirdLine(ordersLines[i], itemsWeights);
                break;
        }
    }
    return orders;
}

// parse order first line = destination
function parseOrderFirstLine(line) {
    let arr = line.split(" ");
    return new Location(arr[0], arr[1]);
}

// parse order second line = numberOfItems
function parseOrderSecondLine(line) {
    return line;
}

// parse order third line = itemsToDeliver
function parseOrderThirdLine(line, itemsWeights) {
    var arr = line.split(" ");
    return arr.map(function(itemType) {
        return new Item(itemType, getItemWeight(itemType, itemsWeights));
    })
}

function getItemWeight(itemType, itemsWeights) {
    return itemsWeights[itemType];
}


function parseFile(filename, done) {
    readFile(filename, function(err, lines) {

        let metaDataLines = lines.splice(0,4);
        var game = parseGameMetaData(metaDataLines);



        let amountOfWarehousesLines = linesPerWarehouse * game.numberOfWarehouses;
        let warehousesLines = lines.splice(0,amountOfWarehousesLines);
        game.warehouses = parseWarehouses(warehousesLines);

        game.numberOfOrders = lines.splice(0,1);

        let orderLines = lines
        let itemsWeights = game.productsWeights
        game.orders = parseOrders(orderLines, itemsWeights);

        game.createDrones();
        lines.length = 0;
        done(null, game);
    })
}

module.exports = parseFile