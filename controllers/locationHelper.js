/**
 * Created by Tomer on 11/02/2016.
 */
var Location = require('../models/location')


module.exports = {
    getDistanceBetweenTwoPoints : function (p1,p2){
        return Math.ceil(Math.sqrt(Math.pow(Math.abs(p1.row - p2.row),2) + Math.pow(Math.abs(p1.column - p2.column),2)));
    }
}

