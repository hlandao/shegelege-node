"use strict";

var parser = require('./parser/parser');
var Location = require('./models/location')
var Warehouse = require('./models/warehouse')
var Order = require('./models/order')
var Game = require('./models/game')
var ControlTower = require('./models/control_tower');
//
//
//
//
//parser('./input/redundancy.in', function(err, game){
//    var controlTower = new ControlTower(game)
//})
parser('./input/redundancy.in', function(err, game){
    var controlTower = new ControlTower(game, './redundancy.txt')
})

parser('./input/mother_of_all_warehouses.in', function(err, game){
    var controlTower = new ControlTower(game, './mother_of_all_warehouses.txt')
})

parser('./input/busy_day.in', function(err, game){
    var controlTower = new ControlTower(game, './busy_day.txt')
})