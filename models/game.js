"use strict";

var Drone = require('./drone');
/**
 * Created by Tomer on 11/02/2016.
 */

class game{
    constructor(rows, cols, numberOfDrones, turns, maxPayload, productsAmount, productsWeights, numberOfWarehouses, warehouses, numberOfOrders, orders, drones){
        this.rows = rows;
        this.cols = cols;
        this.numberOfDrones = numberOfDrones;
        this.turns = turns;
        this.maxPayload = maxPayload;
        this.productsAmount = productsAmount;
        this.productsWeights = productsWeights;
        this.numberOfWarehouses = numberOfWarehouses;
        this.warehouses = warehouses;
        this.numberOfOrders = numberOfOrders;
        this.orders = orders
        this.drones = drones;
    }
    createDrones() {
        this.drones = [];
        for (var i = 0; i < this.numberOfDrones; ++i) {
            this.drones.push(new Drone(i, this.maxPayload));
        }
    }
}

module.exports = game