"use strict";

var _ = require("underscore")
var fs  = require("fs");

class ControlTower {
    constructor(game, outputFileName) {
        this.numberOfRounds = game.turns;
        this.game = game;
        this.outputFileName = outputFileName || "./output.txt"
        this.orderOrdersByPrice();
        this.completedOrders = [];
        this.ordersInProcess = [];
        this.currentRound = 0;

        this.output = [];
        this.run();
    }

    orderOrdersByPrice () {
        var initialOrders = this.game.orders;
        _.each(initialOrders, (order) => {
            this.computePrice(order);
        });
        this.orders = _.sortBy(initialOrders, function (a, b) {
            return a.totalPrice - b.totalPrice;
        });
    }

    findBestWearhouse(item, location) {
        var curMin = 100000, curWearhouse;

        _.each(this.game.warehouses, function (wearhouse) {
            var val = 10000;
            if (wearhouse.itemExists(item)) {
                val = wearhouse.distance(location);
            }

            if (val < curMin) {
                val = curMin;
                curWearhouse = wearhouse;
            }
        });

        item.warehouseForPickup = curWearhouse;
        item.price = curMin;
    }

    computePrice(order) {
        order.itemsToDeliver.forEach(function (item) {
            this.findBestWearhouse(item, order.destination);
            order.totalPrice += item.price;
        }.bind(this));
    }

    run() {
        while (this.numberOfRounds-- > 0) {
            this.tick();
        }

        console.log('This is our solution !!!! \n\n');
        console.log(this.output.length + ' lines, working...please wait');

        var outputFile = this.outputFileName;

        fs.appendFileSync(outputFile, this.output.length + "\n");

        this.output.forEach(function (line) {
            fs.appendFileSync(outputFile, line + "\n");
        });

        console.log('finish :)');
    }

    tick() {
        this.game.drones.forEach(function(drone){
            if(drone.stepsUntilFree == 0){
                var order = this.getNextOrder.call(this);
                if(!order) return;
                var isOrderEmpty = this.processOrderWithDrone(drone, order);
                if(isOrderEmpty){
                    this.emptyOrdersInProcess()
                }
            }
        }.bind(this));

        this.game.drones.forEach(function(drone){
            drone.tick();
        });
    }

    getNextOrder() {
        if (this.ordersInProcess.length == 0) {
            this.ordersInProcess.push(this.orders.shift())
        }

        return this.ordersInProcess[0];
    }

    // return true if order is empty
    processOrderWithDrone(drone, order) {
        for (var i = 0; i < order.itemsToDeliver.length; ++i) {
            let item = order.itemsToDeliver[i];
            let canLoadProduct = drone.canLoadProduct(item);
            if (canLoadProduct) {
                order.itemsToDeliver.splice(i--, 1);
                this.load(drone, item.warehouseForPickup, item.type, 1);
                drone.load(item, order);
                item.warehouseForPickup.unload(item.type);

                this.deliver(drone, order, item.type, 1)
                drone.deliver(item, order);
            } else {
                break;
            }
        }

        if (order.itemsToDeliver.length == 0) return true;
        else return false;
    }

    emptyOrdersInProcess() {
        this.ordersInProcess.length = 0
    }

    load(drone, warehouse, productType, productCount) {
        this.output.push(drone.number + " L " + warehouse.number + " " + productType + " " + productCount);
    }

    unload(drone, warehouse, productType, productCount) {
        var push = this.output.push;
        push(drone.number + " U " + warehouse.number + " " + productType + " " + productCount);
    }

    deliver(drone, order, productType, productCount) {
        this.output.push(drone.number + " D " + order.orderNumber + " " + productType + " " + productCount);
    }

    wait(drone, turns) {
        this.output.push(drone.number + " W " + turns);
    }
}
module.exports = ControlTower;