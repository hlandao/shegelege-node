"use strict";

/**
 * Created by Tomer on 11/02/2016.
 */

class item {
    constructor(type, weight) {
        this.weight = weight;
        this.type = type;
        this.warehouseForPickup = null;
        this.price = 100000;
    }
}

module.exports = item