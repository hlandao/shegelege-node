"use strict";

var Location = require('./location');
var locationHelper = require('../controllers/locationHelper')
/**
 * Created by Tomer on 11/02/2016.
 */

class drone {
    constructor(number, maxWeight) {
        this.currentLocation = new Location(0, 0);
        this.destination = null;
        this.maxWeight = maxWeight;
        this.number = number;
        this.currentWeight = 0;
        this.payload = 0;
        this.isFull = false;
        this.stepsUntilFree = 0;
    }

    canLoadProduct(item) {
        return !!((this.payload + item.weight) <= this.maxWeight);
    }

    load(item, order) {
        this.payload += item.weight;
        var distance = locationHelper.getDistanceBetweenTwoPoints(this.currentLocation, item.warehouseForPickup.location);
        this.stepsUntilFree += distance + 1;
    }

    deliver(item, order) {
        this.destination = order.destination;
        var distance = locationHelper.getDistanceBetweenTwoPoints(order.destination, item.warehouseForPickup.location);
        this.stepsUntilFree += distance;
    }

    tick(){
        if(this.stepsUntilFree > 0){
            this.stepsUntilFree--;
            if(this.stepsUntilFree == 0){
                this.currentLocation = this.destination;
                this.destination = null;
                this.payload = 0;
            }
        }
    }
}

module.exports = drone
