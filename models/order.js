"use strict";
/**
 * Created by Tomer on 11/02/2016.
 */

class order {
    constructor(orderNumber, location, numberOfItems, items) {
        this.destination = location;
        this.numberOfItems = numberOfItems;
        this.itemsToDeliver = items;
        this.orderNumber = orderNumber;
        this.fulfilled = false;
        this.totalPrice = 0;
    }
}


module.exports = order;