"use strict";

var _ = require('underscore');
var locationHelper = require('../controllers/locationHelper.js');
/**
 * Created by Tomer on 11/02/2016.
 */

class warehouse {

    constructor(number, location, inventory) {
        this.number = number;
        this.location = location;
        this.inventory = inventory;
    }

    itemExists(item) {
        return  _.find(this.inventory, (type) => {
            return type == item.type;
        });
    }

    distance(location) {
        return locationHelper.getDistanceBetweenTwoPoints(this.location, location);
    };

    unload(itemType) {
        var index = this.inventory.indexOf(itemType);
        if (index >= 0) {
            this.inventory.splice(index, 1);
        }
    }
}

module.exports = warehouse